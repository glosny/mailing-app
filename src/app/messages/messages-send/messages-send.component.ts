import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-messages-send',
  templateUrl: '../messages-list/messages-list.component.html',
  styleUrls: ['../messages-list/messages-list.component.scss']
})
export class MessagesSendComponent implements OnInit {
  messages: any;

  constructor(private _data: DataService) { }

  deleteMessage(msg) {
    this._data.deleteMessage(msg)
      .subscribe(res => {
        console.log(res)
      })
  }

  readMessage(msg) {
    this._data.patchMessage(msg)
      .subscribe(res => {
        console.log(res)
      })
  }

  ngOnInit() {
    this._data.getAllMessages()
      .subscribe(res => {
        this.messages = res.filter((item) => {
          if (item.recipient == 'Dawid Głośny'){
            return item;
          }
        })
      });
  }
}
