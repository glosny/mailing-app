import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from './data.service';

import { AppComponent } from './app.component';
import { MessagesListComponent } from './messages/messages-list/messages-list.component';
import { MessageComponent } from './messages/message/message.component';
import { NewMessageComponent } from './messages/new-message/new-message.component';
import { MessagesSendComponent } from './messages/messages-send/messages-send.component';

const appRoutes: Routes =[
  { path: '', component: MessagesListComponent },
  { path: 'messages/:id', component: MessageComponent },
  { path: 'send', component: MessagesSendComponent },
  { path: 'new', component: NewMessageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MessagesListComponent,
    MessageComponent,
    NewMessageComponent,
    MessagesSendComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
